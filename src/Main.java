import model.HorParabola;
import model.Line;
import model.VertParabola;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;


public class Main extends Canvas {
	private static Color defaultColor = Color.GRAY;

	private final static int IMAGE_HEIGHT = 800, IMAGE_WIDTH = 800;
	private final static int CANVAS_ABS_SIZE = 10;
	private BufferedImage img;

	// Создаем вертикальные параболы
	private static final VertParabola topRightPar = new VertParabola(0.25, -3, 13);
	private static final VertParabola centerPar = new VertParabola(0.125, -0.75, 1.125);
	private static final VertParabola bottomPar = new VertParabola(-0.25, 0, 0);
	// Создаем горизонтальную параболу
	private static final HorParabola leftPar = new HorParabola(-1, -6, -7);
	// Создаем линию
	private static final Line line = new Line(0.25, 2.75);

	@Override
	public void paint(Graphics g) {
		int x_unit = IMAGE_WIDTH / CANVAS_ABS_SIZE / 2, y_unit = IMAGE_HEIGHT / CANVAS_ABS_SIZE / 2;
		for (int x = 0; x < IMAGE_WIDTH; x++) {
			for (int y = 0; y < IMAGE_HEIGHT; y++) {
				double canv_x = (double) x / x_unit - CANVAS_ABS_SIZE;
				double canv_y = CANVAS_ABS_SIZE - (double) y / y_unit;
				if (x % x_unit == 0 || y % y_unit == 0) {
					img.setRGB(x, y, Color.BLACK.getRGB());
				} else {
					img.setRGB(x, y, getColor(canv_x, canv_y).getRGB());
				}
			}
		}
		g.drawImage(img, 0, 0, null);
		// оси координат
		g.setColor(Color.BLACK);
		((Graphics2D) g).setStroke(new BasicStroke(2));
		g.drawLine(IMAGE_WIDTH / 2, 0, IMAGE_WIDTH / 2, IMAGE_HEIGHT);
		g.drawLine(0, IMAGE_HEIGHT / 2, IMAGE_WIDTH, IMAGE_HEIGHT / 2);
	}

	public Main() {
		img = new BufferedImage(IMAGE_WIDTH, IMAGE_HEIGHT, BufferedImage.TYPE_INT_ARGB);
	}

	public static void main(String[] args) {
		JFrame frame = new JFrame();
		Main canvas = new Main();
		canvas.setSize(IMAGE_WIDTH, IMAGE_HEIGHT);
		frame.add(canvas);
		frame.pack();
		frame.setVisible(true);
	}

	private static void printPoint(double x, double y) {
		Color color = getColor(x, y);
		System.out.printf("( %.2f ; %.2f ) -> %s\n", x, y, color);
	}

	public static Color getColor(double x, double y) {

		if (!leftPar.contains(x, y) && !bottomPar.contains(x, y)
				|| topRightPar.contains(x, y) && !line.contains(x, y)) {
			return Color.YELLOW;
		}

		if (topRightPar.contains(x, y) && line.contains(x, y)
				|| centerPar.contains(x, y) && !line.contains(x, y) && !topRightPar.contains(x, y)
				|| centerPar.contains(x, y) && !topRightPar.contains(x, y) && x > 6) {
			return Color.WHITE;
		}

		if (x < 0 && !centerPar.contains(x, y) && line.contains(x, y)) {
			return Color.GREEN;
		}

		if (centerPar.contains(x, y) && !topRightPar.contains(x, y)
				|| !centerPar.contains(x, y) && line.contains(x, y)
				|| !bottomPar.contains(x, y)
				|| leftPar.contains(x, y) && bottomPar.contains(x, y) && x < -4 && y < -5) {
			return Color.BLUE;
		}


		return defaultColor;
	}


}



