package model;

public class HorParabola {
	public double a, b, c;

	public HorParabola(double a, double b, double c) {
		this.a = a;
		this.b = b;
		this.c = c;
	}

	public boolean contains(double x, double y) {
		return x >= a * y * y + b * y + c;
	}

}
