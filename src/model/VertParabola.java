package model;

public class VertParabola {
	public double a, b, c;

	public VertParabola(double a, double b, double c) {
		this.a = a;
		this.b = b;
		this.c = c;
	}

	public boolean contains(double x, double y) {
		return y >= a * x * x + b * x + c;
	}
}
