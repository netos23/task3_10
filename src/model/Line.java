package model;

public class Line {
	public double k, m;

	public Line(double k, double m) {
		this.k = k;
		this.m = m;
	}

	public boolean contains(double x, double y) {
		return y >= k * x + m;
	}
}
